package main

import (
	"bufio"
	"encoding/json"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"

// debug
//  "fmt"
//  "reflect"
  _ "net/http/pprof"
)

// TODO externalize
const (
  filepath = "lyrics.jsonl"
  seed = 1
  maxLineLength = 50
)

const End = "\n"


var markovModel *MarkovModel

// Prefix is a Markov chain prefix of one or more words.
type Prefix []string

// NewPrefix returns a new Prefix containing the provided words.
func NewPrefix(words ...string) Prefix {
  return Prefix(words)
}

// String returns the Prefix as a string (for use as a map key).
func (p Prefix) String() string {
  return strings.Join(p, " ")
}

type Chain struct {
  Chain     map[string][]string
  PrefixLen int
}

// NewChain returns a new markov chain.
func NewChain(prefixLen int) Chain {
  return Chain {
    Chain: make(map[string][]string),
    PrefixLen: prefixLen,
  }
}

func (c *Chain) Add(prefix Prefix, word string) {
  key := prefix.String()
  c.Chain[key] = append(c.Chain[key], word)
}

// Next returns the next generated word for the given prefix.
func (c Chain) Next(p Prefix) string {
  key := p.String()
  return c.SelectWord(key)
}

// SelectWord returns a random word from a list of words.
func (c Chain) SelectWord(key string) string {
  words := c.Chain[key]
  if len(words) == 0 {
    return End
  }
  idx := rand.Int() % len(words)
  return words[idx]
}

// MarkovModel contains the markov chains used to generate lyrics.
type MarkovModel struct {
	initialWordChain   Chain
	secondWordChain    Chain
	remainingWordChain Chain
}

// NewMarkovModel returns an empty MarkovModel with initialized Chains.
func NewMarkovModel() *MarkovModel {
	return &MarkovModel{
		initialWordChain:   NewChain(0),
		secondWordChain:    NewChain(1),
		remainingWordChain: NewChain(2),
	}
}

func (m *MarkovModel) Build(tracks []Track) {
  for _, t := range tracks {
    m.AddTrack(&t)
  }
}

// AddTrack adds a track to the markov chain.
func (m *MarkovModel) AddTrack(track *Track) {
	lines := track.getLines()
	for _, l := range lines {
		tokens := tokenizeLine(l)
    m.AddLine(tokens)
	}
}

// AddLine adds a tokenized line to the markov model.
func (m *MarkovModel) AddLine(words []string) {
  for i, w := range words {
    switch i {
    case 0:
      p := NewPrefix()
      m.initialWordChain.Add(p, w)

    case 1:
      p := NewPrefix(words[i-1])
      m.secondWordChain.Add(p, w)

    default:
      p := NewPrefix(words[i-2], words[i-1])
      m.remainingWordChain.Add(p, w)
    }
  }
}

// GenerateLines generates a specified number of lines of lyrics.
func (m MarkovModel) GenerateLines(cnt int) string {
	lines := ""
	for i := 0; i < cnt; i++ {
		lines += m.GenerateLine()
	}
	return lines
}

// GenerateLine generates a line of lyrics.
func (m MarkovModel) GenerateLine() string {
  words := make([]string, maxLineLength, maxLineLength)

  // Generate the first word
  prefix1 := NewPrefix()
	word1 := m.initialWordChain.Next(prefix1)
	words[0] = word1

  // Generate the second word
  prefix2 := NewPrefix(word1)
	word2 := m.secondWordChain.Next(prefix2)
	words[1] = word2

  // Generate the remainder of the line
  i := 2
  nextWord := word2
	for nextWord != End && i < maxLineLength {
    prefix := NewPrefix(word1, word2)
		nextWord = m.remainingWordChain.Next(prefix)
		words[i] = nextWord

    i += 1
		word1 = word2
		word2 = nextWord
	}

	return strings.Join(words[:], " ")
}

type Track struct {
	Artist string
	Album  string
	Title  string
	Lyrics string
}

func (t Track) getLines() []string {
	lines := strings.Split(t.Lyrics, "\n")

	// remove empty lines
	result := []string{}
	for _, l := range lines {
		if len(strings.TrimSpace(l)) != 0 {
			result = append(result, l)
		}
	}

	return result
}

func tokenizeLine(line string) []string {
	line = normalizeLine(line)
	tokensWithSomeEmpty := strings.Split(line, " ")

	// remove empty tokens
	tokens := []string{}
	for _, t := range tokensWithSomeEmpty {
		if len(strings.TrimSpace(t)) != 0 {
			tokens = append(tokens, t)
		}
	}

	return tokens
}

func normalizeLine(line string) string {
	line = strings.ReplaceAll(line, ",", "")
	line = strings.ReplaceAll(line, ";", "")
	line = strings.ReplaceAll(line, ".", "")
	line = strings.ReplaceAll(line, `"`, "")
	line = strings.ReplaceAll(line, `(`, "")
	line = strings.ReplaceAll(line, `)`, "")
	return line
}

func readTracks() []Track {
	f, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	tracks := []Track{}
	fs := bufio.NewScanner(f)
	for fs.Scan() {
		var track Track
		json.Unmarshal([]byte(fs.Text()), &track)
		tracks = append(tracks, track)
	}

	return tracks
}

var templates = template.Must(template.ParseFiles("index.html"))

const (
  defaultLines = 10
)


func lyricsHandler(w http.ResponseWriter, r *http.Request) {
	lines, _ := strconv.Atoi(r.FormValue("lines"))
  if lines != 10 && lines != 25 && lines != 50  {
		lines = defaultLines
	}

	lyrics := strings.Split(markovModel.GenerateLines(lines), "\n")

	v := TemplateData{
    Lines: lines,
		Options: albums,
		Lyrics:  lyrics,
	}

  err := templates.ExecuteTemplate(w, "index.html", v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	var lyrics []string
	switch r.Method {
	case http.MethodPost:
    //  lines := r.FormValue("lines")

		//lines, selectedAlbums := parseForm(r)
		//lyrics := generateLyrics(lines, selectedAlbums)
	case http.MethodGet:
	default:
    lyrics = strings.Split(markovModel.GenerateLines(50), "\n")
	}
	v := TemplateData{
		Options: albums,
		Lyrics:  lyrics,
	}
	err := templates.ExecuteTemplate(w, "index.html", v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

type TemplateData struct {
  Lines   int
	Options []string
	Lyrics  []string
}

var a TemplateData = TemplateData{Options: albums}

func main() {
	log.Println("howdy")
  rand.Seed(seed)
  markovModel = NewMarkovModel()
  markovModel.Build(readTracks())

	http.HandleFunc("/", lyricsHandler)
	log.Println("ready to roll")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

var albums []string = []string{
	"The Freewheelin' Bob Dylan",
	"Another Side Of Bob Dylan",
	"The Times They Are A-Changin'",
	"Bringing It All Back Home",
	"Highway 61 Revisited",
	"Blonde On Blonde",
	"John Wesley Harding",
	"Nashville Skyline",
	"New Morning",
	"Self Portrait",
	"Dylan",
	"Pat Garrett & Billy The Kid",
	"Planet Waves",
	"Blood On The Tracks",
	"The Basement Tapes",
	"Desire",
	"Street Legal",
	"Slow Train Coming",
	"Saved",
	"Shot Of Love",
	"Infidels",
	"Empire Burlesque",
	"Knocked Out Loaded",
	"Down In The Groove",
	"Oh Mercy",
	"Under The Red Sky",
	"Good As I Been To You",
	"World Gone Wrong",
	"Time Out Of Mind",
	"Love And Theft",
	"Modern Times",
	"Together Through Life",
	"Christmas In The Heart",
	"Tempest",
	"Shadows In The Night",
	"Fallen Angels",
	"Triplicate",
	"",
	"Bob Dylan",
}
